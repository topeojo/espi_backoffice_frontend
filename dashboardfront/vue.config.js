const webpack = require("webpack");
const path = require("path");
const isProd = process.env.NODE_ENV === "production";

module.exports = {
  publicPath: isProd ? "/" : "",
  outputDir: path.resolve(__dirname, "../server/dist"),
  configureWebpack: {
    // Set up all the aliases we use in our app.
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 6
      })
    ]
  },
  pwa: {
    name: "ESPI BACKOFFICE",
    iconPaths: {
      favicon32: "./img/fisshboneandlestrlogo.png",
      favicon16: "./img/fisshboneandlestrlogo.png",
      appleTouchIcon: "./img/fisshboneandlestrlogo.png",
      maskIcon: "./img/fisshboneandlestrlogo.png",
      msTileImage: "./img/fisshboneandlestrlogo.png"
    },
    themeColor: "#172b4d",
    msTileColor: "#172b4d",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "#172b4d"
  },
  css: {
    // Enable CSS source maps.
    sourceMap: process.env.NODE_ENV !== "production"
  }
};
// module.exports = {
//   publicPath: isProd ? "/vue-argon-dashboard/" : "",
//   configureWebpack: {
//     // Set up all the aliases we use in our app.
//     plugins: [
//       new webpack.optimize.LimitChunkCountPlugin({
//         maxChunks: 6
//       })
//     ]
//   },
//   pwa: {
//     name: "Vue Argon Dashboard",
//     themeColor: "#172b4d",
//     msTileColor: "#172b4d",
//     appleMobileWebAppCapable: "yes",
//     appleMobileWebAppStatusBarStyle: "#172b4d"
//   },
//   css: {
//     // Enable CSS source maps.
//     sourceMap: process.env.NODE_ENV !== "production"
//   }
// };
