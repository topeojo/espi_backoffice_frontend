import Vue from "vue";
import Router from "vue-router";
import DashboardLayout from "@/layout/DashboardLayout";
import AuthLayout from "@/layout/AuthLayout";

import Login from "@/views/Login";

/* ESPI CDO VIEWS */
import Dashboard_ESPI_CDO from "@/views/ESPI_CDO/Dashboard";

/* FELA WALLET VIEWS */
import Dashboard from "@/views/FELA_WALLET/Dashboard";
import MerchantDashboard from "@/views/FELA_WALLET/MerchantViews/Dashboard_Merchant";
import Profile from "@/views/FELA_WALLET/UserProfile";
import Tables from "@/views/FELA_WALLET/Tables";
import CreateUser from "@/views/FELA_WALLET/CreateUser";
import RolePriviledges from "@/views/FELA_WALLET/RolePriviledges";
import GenerateReport from "@/views/FELA_WALLET/GenerateReport";
import GenerateReport1 from "@/views/FELA_WALLET/GenerateReport1";
import LandingPage from "@/views/LandingPage";
import NotFoundPage from "@/views/NotFoundPage";
import UserReport from "@/views/FELA_WALLET/Reports/UsersReport";
import TransactionsReport from "@/views/FELA_WALLET/Reports/TransactionsReport";
import PaymentsReport from "@/views/FELA_WALLET/Reports/PaymentsReport";
import WalletsReport from "@/views/FELA_WALLET/Reports/WalletsReport";
import CashoutTransferReport from "@/views/FELA_WALLET/Reports/CashoutTransferReport";

/* ESPI PAYMENTS VIEWS */
import Dashboard_ESPI_PAYMENTS from "@/views/ESPI_PAYMENTS/Dashboard";

Vue.use(Router);

let router = new Router({
  linkExactActiveClass: "active",
  mode: "history",
  routes: [
    {
      path: "/",
      redirect: "login",
      component: AuthLayout,
      children: [
        {
          path: "/login",
          name: "login",
          component: Login
        },
        {
          path: "/services",
          component: LandingPage,
          meta: {
            requiresAuth: true
          }
        }
      ]
    },
    {
      path: "/board",
      component: DashboardLayout,
      meta: {
        requiresAuth: true
      },
      //ESPI WALLETS
      children: [
        {
          path: "/dashboard",
          name: "dashboard",
          component: Dashboard
        },
        {
          path: "/mDashboard",
          name: "dashboard_merchant",
          component: MerchantDashboard
        },
        {
          path: "/profile",
          name: "profile",
          component: Profile
        },
        {
          path: "/tables",
          name: "tables",
          component: Tables
        },
        {
          path: "/createUser",
          name: "create new user",
          component: CreateUser
        },
        {
          path: "/rolePriviledges",
          name: "Setup Role Priviledges",
          component: RolePriviledges
        },
        {
          path: "/usersReport",
          name: "Users Report",
          component: UserReport
        },
        {
          path: "/transactionsReport",
          name: "Transactions Report",
          component: TransactionsReport
        },
        {
          path: "/paymentsReport",
          name: "Payments Report",
          component: PaymentsReport
        },
        {
          path: "/walletsReport",
          name: "Wallets Report",
          component: WalletsReport
        },
        {
          path: "/cashoutReport",
          name: "Cashout Transfers Report",
          component: CashoutTransferReport
        },
        {
          path: "/generateReport",
          name: "Generate Report Using Redis String",
          component: GenerateReport
        },
        {
          path: "/generateReport1",
          name: "Generate Report Using Redis List",
          component: GenerateReport1
        }
      ]
    },
    //ESPI CDO
    {
      path: "/board_cdo",
      redirect: "dashboard_cdo",
      component: DashboardLayout,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: "/dashboard_cdo",
          name: "dashboard_cdo",
          component: Dashboard_ESPI_CDO
        }
      ]
    },
    //ESPI PAYMENTS
    {
      path: "/board_espi_payments",
      redirect: "dashboard_espi_payments",
      component: DashboardLayout,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: "/dashboard_espi_payments",
          name: "dashboard_espi_payments",
          component: Dashboard_ESPI_PAYMENTS
        }
      ]
    },
    {
      path: "*",
      name: "404",
      component: NotFoundPage
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("jwt") == null) {
      next({
        path: "/login"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
