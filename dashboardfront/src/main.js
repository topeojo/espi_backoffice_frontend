/* eslint-disable */
import Vue from "vue";
import App from "./App.vue";
import router from "../router";
import "./registerServiceWorker";
import ArgonDashboard from "./plugins/argon-dashboard";
import axios from "axios";
import moment from "moment";
import VueSocketIO from "vue-socket.io-extended";
import io from "socket.io-client";
import jwt from "jsonwebtoken";

console.log(process.env.VUE_APP_MY_ENV);
Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.$moment = moment;
Vue.prototype.$jwt = jwt;

Vue.use(ArgonDashboard);

const socket = io(process.env.VUE_APP_SOCKET_SERVER);
Vue.use(VueSocketIO, socket);

Vue.mixin({
  data: function() {
    return {
      globalUserPriviledges: "",
      globalUserRoleId: ""
    };
  },
  methods: {
    globalSetupUsersPriviledge() {
      this.$http
        .get(`${process.env.VUE_APP_SOCKET_SERVER}/getRolePriviledges`)
        .then(response => {
          let data = response.data;
          let userRoleId = jwt.decode(
            localStorage.getItem("jwt"),
            process.env.VUE_APP_JWT_SECRET
          ).userDetail.userRole;
          this.globalUserRoleId = userRoleId;
          this.globalUserPriviledges = data.filter(item => {
            return item.roleId == userRoleId;
          });
        })
        .catch(err => {
          console.error(err);
        });
    },
    camelize(str) {
      return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        return index == 0 ? match.toLowerCase() : match.toUpperCase();
      });
    },
    formatNumber(num) {
      if (typeof num === "string") {
        num = parseInt(num, 10);
      }
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }
  }
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
