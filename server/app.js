"use strict";
const express = require("express");
const DB = require("./config/db");
const AppConfig = require("./config");
// const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");
const redis = require("redis");
const axios = require("axios");
const history = require("connect-history-api-fallback");
const cors = require("cors");
const helmet = require("helmet");
const { redisClient } = require("./config/redisConnectConfig");
const { getEspiReport } = require("./config/espi_apis");
const db = new DB();
const app = express();
const API_URL = "https://jsonplaceholder.typicode.com";

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(history());
app.use(express.static("dist"));

app.use(cors());
app.use(helmet());

app.post("/registerUser", function(req, res) {
  db.insertUser(
    [
      req.body.firstName,
      req.body.lastName,
      req.body.email,
      req.body.password,
      req.body.userRole
    ],
    err => {
      if (err) {
        console.log(err);
        return res
          .status(500)
          .send("There was a problem registering the user.");
      }
      console.log("New user registered successfully");
      res.status(200).send("User registered successfully");
    }
  );
});

app.post("/authenticateAdmin", (req, res) => {
  console.log(req.body);
  db.selectByEmail(req.body.email, (err, user) => {
    if (err) return res.status(500).send("Error on the server.");

    let passwordIsValid = req.body.password === user[0].userPass;
    if (!passwordIsValid) {
      console.log("Admin Aunthentication Failed");
      return res.status(401).send("Incorrect Password");
    } else {
      console.log("Admin Aunthentication Successful");
      return res.status(200).send("Admin Authenticated");
    }
  });
});

app.post("/login", (req, res) => {
  db.selectByEmail(req.body.email, (err, user) => {
    if (err) {
      console.log(err);
      return res.status(500).send("Error on the server.");
    }
    if (user.length === 0) {
      console.log("No user found");
      return res.status(400).send("No user found");
    }

    console.log(user);

    // let passwordIsValid = bcrypt.compareSync(
    //   req.body.password,
    //   user[0].user_pass
    // );
    let passwordIsValid = req.body.password === user[0].userPass;
    if (!passwordIsValid) {
      return res.status(401).send({ auth: false, token: null });
    }

    let token = jwt.sign({ userDetail: user[0] }, AppConfig.App.SECRET);

    res.status(200).send({ auth: true, token: token });
  });
});

app.post("/insertIntoRolesPriviledges", (req, res) => {
  db.insertIntoRolesPriviledges(
    [
      req.body.userId,
      req.body.priviledgeId,
      req.body.status,
      req.body.createdAt,
      req.body.updatedAt
    ],
    err => {
      if (err) {
        console.log(err);
        return res
          .status(500)
          .send("There was a problem inserting new priviledge for the user.");
      }
      console.log("User Priviledge inserted successfully");
      res.status(200).send("User Priviledge inserted successfully");
    }
  );
});

app.post("/insertNewRole", (req, res) => {
  db.insertNewRole([req.body.roleName], err => {
    if (err) {
      console.log(err);
      return res
        .status(500)
        .send("There was a problem setting up the new role");
    }
    console.log("New Role created successfully");
    res.status(200).send("New Role created successfully");
  });
});

app.post("/insertNewPriviledge", (req, res) => {
  db.insertNewPriviledge([req.body.priviledgeTitle], err => {
    if (err) {
      console.log(err);
      return res
        .status(500)
        .send("There was a problem setting up the new priviledge");
    }
    console.log("New priviledge created successfully");
    res.status(200).send("New priviledge created successfully");
  });
});

app.post("/insertUserProfile", (req, res) => {
  db.getUserProfile(req.body.profile[0], (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).send("There was a problem inserting user profile");
    }

    if (result.length === 0) {
      //Insert a new User profile
      db.insertUserProfile(req.body.profile, err => {
        if (err) {
          console.log(err);
          return res
            .status(500)
            .send("There was a problem inserting user profile");
        }

        redisClient.hmset(`userProfile/${req.body.profile[0]}`, {
          userId: `${req.body.profile[0]}`,
          userName: `${req.body.profile[1]}`,
          email: `${req.body.profile[2]}`,
          firstName: `${req.body.profile[3]}`,
          lastName: `${req.body.profile[4]}`,
          address: `${req.body.profile[5]}`,
          city: `${req.body.profile[6]}`,
          country: `${req.body.profile[7]}`,
          zipCode: `${req.body.profile[8]}`,
          about: `${req.body.profile[9]}`
        });
        console.log("New user profile added");
        return res.status(200).send("User profile inserted successfully");
      });
    } else if (result.length > 0) {
      //Update the User Profile
      let modifiedBody = req.body.profile;
      modifiedBody.push(req.body.profile[0]);
      db.updateUserProfile(modifiedBody, err => {
        if (err) {
          console.log(err);
          return res
            .status(500)
            .send("There was a problem inserting user profile");
        }

        redisClient.del(
          `userProfile/${req.body.profile[0]}`,
          (err, response) => {
            console.log(response);
            redisClient.hmset(`userProfile/${req.body.profile[0]}`, {
              userId: `${req.body.profile[0]}`,
              userName: `${req.body.profile[1]}`,
              email: `${req.body.profile[2]}`,
              firstName: `${req.body.profile[3]}`,
              lastName: `${req.body.profile[4]}`,
              address: `${req.body.profile[5]}`,
              city: `${req.body.profile[6]}`,
              country: `${req.body.profile[7]}`,
              zipCode: `${req.body.profile[8]}`,
              about: `${req.body.profile[9]}`
            });
          }
        );

        console.log("User profile updated successully");
        return res.status(200).send("User profile inserted successfully");
      });
    }
  });
});

app.post("/updateRolePriviledge", (req, res) => {
  db.updateRolePriviledge(
    [
      req.body.status,
      req.body.updatedAt,
      req.body.userId,
      req.body.priviledgeId
    ],
    err => {
      if (err) {
        console.log(err);
        return res
          .status(500)
          .send("There was a problem disabling this priviledge for the user.");
      }
      if (req.body.status === "active") {
        console.log("User Priviledge activated successfully");
      } else if (req.body.status === "disabled") {
        console.log("User Priviledge disabled successfully");
      }

      return res.status(200).send("User Priviledge disabled successfully");
    }
  );
});

app.get("/getDashboardMenus", (req, res) => {
  db.getDashBoardMenus((err, menus) => {
    if (err) return res.status(500).send("Error getting menus");

    res.status(200).send(menus);
  });
});

app.get("/getRoles", (req, res) => {
  db.getRoles((err, roles) => {
    if (err) return res.status(500).send("Error getting information");

    return res.status(200).send(roles);
  });
});

app.get("/getPriviledges", (req, res) => {
  db.getPriviledges((err, priviledges) => {
    if (err) return res.status(500).send("Error getting information");

    return res.status(200).send(priviledges);
  });
});

app.get("/getRolePriviledges", (req, res) => {
  db.getRolePriviledges((err, priviledges) => {
    if (err) return res.status(500).send("Error getting information");

    return res.status(200).send(priviledges);
  });
});

// router.get("/getRolePriviledges", (req, res) => {
//   db.getRolePriviledges((err, priviledges) => {
//     if (err) return res.status(500).send("Error getting information");

//     let me = new Promise((resolve, reject) => {
//       resolve(res.status(200).send("A"));
//       return;
//     });

//     me.then(() => {
//       console.log("Got here");
//       res.status(200).send("B");
//     });
//   });
// });

app.post("/getUserProfile", (req, res) => {
  redisClient.exists(`userProfile/${req.body.userId}`, (err, response) => {
    if (response === 0) {
      console.log("Fetching User Profile from the DB");
      db.getUserProfile(req.body.userId, (err, result) => {
        if (err) return res.status(500).send("Error getting user profile");

        console.log(result);
        if (result.length != 0) {
          redisClient.hmset(`userProfile/${req.body.userId}`, result[0]);
        }
        return res.status(200).send(result);
      });
    } else if (response === 1) {
      console.log("Fetching User Profile from Redis");
      redisClient.hgetall(`userProfile/${req.body.userId}`, (err, reply) => {
        if (err) return res.status(500).send("Error getting user profile");

        console.log("HGET REPLY:");
        console.log(reply);
        return res.status(200).send([reply]);
      });
    }
  });
});

app.get("/getUser", (req, res) => {
  let userId = req.query.user;
  // data validation
  // const data = RequestValidator.validate(req, expectedParams)
  // forward request to a handler
  // UserHandler.getDetails(data);
  console.log(userId);
  redisClient.get(`users/${userId}`, (err, result) => {
    if (result == null) {
      console.log("Fetching from API");
      axios.get(`${API_URL}/users/${userId}`).then(json => {
        console.log(json.data);
        redisClient.setex(`users/${userId}`, 180, JSON.stringify(json.data));
        return res.status(200).send(json.data);
      });
    } else {
      console.log("Fetching from Redis");
      return res.status(200).send(JSON.parse(result));
    }
  });
});

app.get("/getPosts", (req, res) => {
  let range = req.query.range;
  let start = (range - 1) * 10;
  let end = start + 9;

  redisClient.exists("posts", (err, existRes) => {
    console.log("Reply from exist == " + existRes);
    if (existRes === 0) {
      axios.get(`${API_URL}/posts`).then(response => {
        console.log("Fetching Info from API");
        for (let post of response.data) {
          redisClient.rpush("posts", JSON.stringify(post));
        }

        redisClient.expire("posts", 180);

        redisClient.lrange("posts", start, end, (err, reply) => {
          if (err) throw err;
          let responseObj = [];
          let responseKeys = [];

          for (let post of reply) {
            let postObj = JSON.parse(post);
            responseObj.push(postObj);
          }
          for (let key in responseObj[0]) {
            responseKeys.push(key);
          }

          return res
            .status(200)
            .send({ dataPosts: responseObj, dataKeys: responseKeys });
        });
      });
    } else if (existRes === 1) {
      console.log("Fetching Info from Redis");
      redisClient.lrange("posts", start, end, (err, reply) => {
        if (err) throw err;
        let responseObj = [];
        let responseKeys = [];

        for (let post of reply) {
          let postObj = JSON.parse(post);
          responseObj.push(postObj);
        }
        for (let key in responseObj[0]) {
          responseKeys.push(key);
        }
        return res
          .status(200)
          .send({ dataPosts: responseObj, dataKeys: responseKeys });
      });
    }
  });

  console.log("Done adding to redis");
});

// router.get("/getMockUserDetails", (req, res) => {
//   db.getMockUserDetails((err, data) => {
//     if (err) return res.status(500).send("Error getting information");

//     let responseObj = data;
//     let responseKeys = [];

//     for (let key in responseObj[0]) {
//       responseKeys.push(key);
//     }
//     return res
//       .status(200)
//       .send({ dataPosts: responseObj, dataKeys: responseKeys });
//   });
// });

app.post("/getEspiReport", async (req, res) => {
  try {
    console.log(JSON.stringify(req.body, null, 2));
    let response = await getEspiReport(req.body, req.query.reportType);
    console.log(response.data.data);
    console.log("****I ran After!");

    let responseObj = response.data.data.rows;
    let responseKeys = [];
    let totalRecords = response.data.data.totalRecords;

    for (let key in responseObj[0]) {
      responseKeys.push(key);
    }

    return res.status(200).send({
      dataPosts: responseObj,
      dataKeys: responseKeys,
      totalDataRecords: totalRecords
    });
  } catch (error) {
    console.error(error);
  }
});

app.get("/getMockUserDetails", (req, res) => {
  redisClient.exists("mockUserDetails", (err, result) => {
    if (result === 0) {
      console.log("Fetching Mock User Details from the DB and adding to cache");
      db.getMockUserDetailsLimited((err, data) => {
        if (err) return res.status(500).send("Error getting information");

        let responseObj = data;
        let responseKeys = [];

        for (let key in responseObj[0]) {
          responseKeys.push(key);
        }

        responseObj.forEach(value => {
          let stringedValue = JSON.stringify(value);
          redisClient.lpush("mockUserDetails", stringedValue);
        });

        return res
          .status(200)
          .send({ dataPosts: responseObj, dataKeys: responseKeys });
      });
    } else if (result === 1) {
      console.log("Fetching Mock User Details from the Cache");
      redisClient.lrange("mockUserDetails", 0, -1, (err, result) => {
        let parsedResult = result.map(value => {
          return JSON.parse(value);
        });

        let responseKeys = [];

        for (let key in parsedResult[0]) {
          responseKeys.push(key);
        }

        return res
          .status(200)
          .send({ dataPosts: parsedResult, dataKeys: responseKeys });
      });
    }
  });
});

app.post("/getMockUserDetailsQueried", (req, res) => {
  db.getMockUserDetailsDateQueried(
    [req.body.dateFrom, req.body.dateTo],
    (err, data) => {
      if (err) return res.status(500).send("Error getting information");

      let responseObj = data;
      let responseKeys = [];

      for (let key in responseObj[0]) {
        responseKeys.push(key);
      }
      return res
        .status(200)
        .send({ dataPosts: responseObj, dataKeys: responseKeys });
    }
  );
});

let port = AppConfig.App.PORT || 3000;

let server = app.listen(port, () => {
  console.log("Express server listening on port " + port);
});

/* THIS IS THE SOCKET IO SIDE OF THE SERVER */
const io = require("socket.io")(server);

io.on("connection", socket => {
  console.log("New User Connected");

  socket.on("getNewReport", data => {
    console.log("Got request to fetch updated mock user details data");
    db.getMockUserDetails((err, report) => {
      redisClient.lrange("mockUserDetails", 0, -1, (err, result) => {
        console.log(report.length);
        console.log(result.length);
        if (report.length > result.length) {
          console.log("There are new updated data from the DB");
          let newUpdatedData = report.slice(result.length);
          // console.log(newUpdatedData);

          newUpdatedData.forEach(value => {
            let stringedValue = JSON.stringify(value);
            redisClient.lpush("mockUserDetails", stringedValue);
          });

          redisClient.lrange("mockUserDetails", 0, -1, (err, result) => {
            let parsedResult = result.map(value => {
              return JSON.parse(value);
            });

            let responseKeys = [];

            for (let key in parsedResult[0]) {
              responseKeys.push(key);
            }

            console.log(parsedResult);

            setTimeout(() => {
              socket.emit("obtainNewReport", {
                dataPosts: parsedResult,
                dataKeys: responseKeys,
                checkUpdate: "new update"
              });
            }, 7000);
          });
        } else if (report.length === result.length) {
          console.log(
            "There is no new data from the DB for us to update our cache with"
          );
          setTimeout(() => {
            socket.emit("obtainNewReport", {
              checkUpdate: "no update"
            });
          }, 7000);
        }
      });

      // let responseObj = report;
      // let responseKeys = [];

      // for (let key in responseObj[0]) {
      //   responseKeys.push(key);
      // }
      // setTimeout(() => {
      //   socket.emit("obtainNewReport", {
      //     dataPosts: responseObj,
      //     dataKeys: responseKeys,
      //     dataFrom: "2019-01-01",
      //     dataTo: "2019-04-30"
      //   });
      // }, 7000);
    });
  });

  setInterval(() => {
    socket.emit("welcome", "You are still connected!");
  }, 5000);
});
