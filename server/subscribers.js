const redis = require("redis");

const redisClient = redis.createClient(6379);

redisClient.on("connect", () => {
  console.log("subscriber is connected");
});

redisClient.on("message", (channel, message) => {
  switch (channel) {
    case "notification":
      console.log(message + "1234");
      break;
    case "push":
      console.log(message + "0909");
      break;
    default:
      console.log("there is no notification yet");
  }
});

redisClient.subscribe("notification", () => {
  console.log("I am subscribed");
});
redisClient.subscribe("push", () => {
  console.log("I am subscribed");
});
