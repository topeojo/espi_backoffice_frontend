const promisify = require("bluebird");
const redis = require("redis");
const { REDIS } = require("./index");

promisify.promisifyAll(require("redis"));

let redisClient = null;

if (REDIS.REDIS_ENV === "staging") {
  redisClient = redis.createClient(REDIS.REDIS_PORT, REDIS.REDIS_HOST);

  redisClient.auth(REDIS.REDIS_AUTH, (err, resp) => {
    console.log(`Auth passed ${resp}`);
  });

  redisClient.on("connect", () => {
    console.log("Server connected to the Redis DB");
  });
  redisClient.on("error", err => {
    console.error("Error connecting to Redis Server" + err);
  });
} else if (REDIS.REDIS_ENV === "local") {
  redisClient = redis.createClient(6379);
  redisClient.on("connect", () => {
    console.log("Server connected to the Redis DB");
  });
  redisClient.on("error", err => {
    console.error("Error connecting to Redis Server" + err);
  });
}

module.exports = { redisClient };
