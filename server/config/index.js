const path = require("path");

const ENV = process.env.NODE_ENV || "development";

require("dotenv").config({
  path: path.resolve(__dirname, `../${ENV}.env`)
});

const Config = Object.freeze({
  App: {
    ENV,
    PORT: process.env.PORT,
    SECRET: process.env.SECRET,
    ESPI_PUBLIC_KEY: process.env.ESPI_PUBLIC_KEY,
    EPSI_PRIVATE_KEY: process.env.ESPI_PRIVATE_KEY,
    ESPI_BASE_URL:
      ENV === "development"
        ? process.env.ESPI_STAGING_BASEURL
        : process.env.ESPI_PRODUCTION_BASEURL
  },
  MYSQL: {
    HOST: process.env.SQL_HOST,
    USER: process.env.SQL_USER,
    PASSWORD: process.env.SQL_PASSWORD,
    DATABASE: process.env.SQL_DATABASE
  },
  REDIS: {
    REDIS_ENV: process.env.REDIS_ENV
  }
});

console.log(Config.App.ESPI_BASE_URL);
module.exports = Config;
