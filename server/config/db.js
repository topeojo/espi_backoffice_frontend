"use strict";

// const sqlite3 = require('sqlite3').verbose()
const mysql = require("mysql");
const { MYSQL: sqlConfig } = require(".");

class DB {
  constructor() {
    this.con = this.connectToDb();
  }

  connectToDb() {
    let con = mysql.createConnection({
      host: sqlConfig.HOST,
      user: sqlConfig.USER,
      password: sqlConfig.PASSWORD,
      database: sqlConfig.DATABASE
    });

    con.connect(err => {
      if (err) throw err;
      console.log("Server connected to mySQL DB");
      // this.createTable(con);
    });

    return con;
  }

  insertUser(user, callback) {
    let [firstName, lastName, email, password, userRole] = user;
    console.log(email, password, userRole);
    let sql = "INSERT INTO user (email, userPass, userRole) VALUES (?,?,?)";
    this.con.query(sql, [email, password, userRole], err => {
      if (err) console.log(err);
      this.con.query(
        `select id from user where email="${email}" `,
        (err, row) => {
          if (err) console.log(err);
          this.con.query(
            `insert into userProfile(userId, userName, firstName, lastName, email) values (${
              row[0].id
            }, '${firstName[0] +
              lastName}','${firstName}','${lastName}', '${email}')`,
            err => {
              return callback(err);
            }
          );
        }
      );
    });
  }

  insertIntoRolesPriviledges(values, callback) {
    let sql =
      "INSERT INTO rolepriviledges(roleId, priviledgeId, status, createdAt, updatedAt) VALUES (?,?,?,?,?)";
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  insertNewRole(values, callback) {
    let sql = "insert into roles(roleName) values (?)";
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  insertNewPriviledge(values, callback) {
    let sql = "insert into priviledges(priviledge) values (?)";
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  insertUserProfile(values, callback) {
    let sql =
      "insert into userprofile(userId, userName, email, firstName, lastName, address, city, country, zipCode, about) values (?,?,?,?,?,?,?,?,?,?)";
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  updateRolePriviledge(values, callback) {
    let sql = `update rolepriviledges set status = ?, updatedAt = ? where roleId = ? and priviledgeId = ?`;
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  updateUserProfile(values, callback) {
    let sql = `update userprofile set userId = ?, userName = ?, email = ?, firstName = ?, lastName = ?, address = ?, city = ?, country = ?, zipCode = ?, about = ? where userId = ?`;
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  selectByEmail(email, callback) {
    return this.con.query(
      `SELECT * FROM user WHERE email = ?`,
      [email],
      function(err, row) {
        callback(err, row);
      }
    );
  }

  getDashBoardMenus(callback) {
    let sql = "select * from dashboardmenus order by id";
    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getRoles(callback) {
    let sql = "select id,roleName from roles order by id";
    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getPriviledges(callback) {
    let sql = "select id,priviledge from priviledges order by id";
    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getRolePriviledges(callback) {
    let sql = `select r.roleName, rp.roleId, rp.priviledgeId, rp.status, p.priviledge
    from rolepriviledges rp
    left join priviledges p on rp.priviledgeId = p.id
    left join roles r on rp.roleId = r.id order by rp.roleId `;

    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getUserProfile(values, callback) {
    let sql = `select * from userprofile where userId = ?`;
    return this.con.query(sql, [values], (err, row) => {
      callback(err, row);
    });
  }

  getMockUserDetails(callback) {
    let sql = `select * from mockuserinfo order by id`;
    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getMockUserDetailsLimited(callback) {
    let sql = `select * from mockuserinfo order by id limit 2`;
    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getMockUserDetailsDateQueried(values, callback) {
    let sql = `select * from mockuserinfo where createdOn >= ? and createdOn <= ? order by id`;
    return this.con.query(sql, values, (err, row) => {
      callback(err, row);
    });
  }
}

module.exports = DB;
