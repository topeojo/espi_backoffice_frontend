const axios = require("axios");
const AppConfig = require(".");
const crypto = require("crypto");

let createHash = (algo, message, digest = "hex") => {
  let hash = crypto.createHash(algo);
  return hash.update(message).digest(digest);
};

let getEspiReport = (message, reportType) => {
  return axios.post(
    `${AppConfig.App.ESPI_BASE_URL}/api/businesses/v1/${reportType}`,
    message,
    {
      headers: {
        Authorization: `Bearer ${AppConfig.App.ESPI_PUBLIC_KEY}:${createHash(
          "sha512",
          JSON.stringify(message) + AppConfig.App.EPSI_PRIVATE_KEY
        )}`
      }
    }
  );
};

module.exports = {
  getEspiReport
};
