const axios = require("axios");
const AppConfig = require("./config/index");
const crypto = require("crypto");

let createHash = (algo, message, digest = "hex") => {
  let hash = crypto.createHash(algo);
  return hash.update(message).digest(digest);
};

let message = {
  pagination: {
    limit: 10,
    page: 1
  }
};

axios
  .post(`${AppConfig.App.ESPI_BASE_URL}/api/businesses/v1/wallets`, message, {
    headers: {
      Authorization: `Bearer ${AppConfig.App.ESPI_PUBLIC_KEY}:${createHash(
        "sha512",
        JSON.stringify(message) + AppConfig.App.EPSI_PRIVATE_KEY
      )}`
    }
  })
  .then(resp => {
    console.log(JSON.stringify(resp.data, null, 2));
  })
  .catch(console.log);
