const redis = require("redis");

const redisClient = redis.createClient(6379);

redisClient.on("connect", () => {
  console.log("publisher is connected");
});

function main() {
  setInterval(() => {
    redisClient.publish("notification", "Heelo!");
    redisClient.publish("push", "Ok, let us see!");
  }, 3000);
}

main();
