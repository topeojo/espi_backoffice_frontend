-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: espi_backoffice_frontend
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.15-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dashboardmenus`
--

DROP TABLE IF EXISTS `dashboardmenus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dashboardmenus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `priviledgeId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `priviledgeId` (`priviledgeId`),
  CONSTRAINT `fk_menuPriviledge` FOREIGN KEY (`priviledgeId`) REFERENCES `priviledges` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboardmenus`
--

LOCK TABLES `dashboardmenus` WRITE;
/*!40000 ALTER TABLE `dashboardmenus` DISABLE KEYS */;
INSERT INTO `dashboardmenus` VALUES (1,'Dashboard','ni ni-tv-2 text-primary','/dashboard',11),(2,'User Profile','ni ni-single-02 text-yellow','/profile',12),(3,'Tables','ni ni-bullet-list-67 text-red','/tables',4),(4,'Create New User','ni ni-badge text-blue','/createUser',3),(5,'Role Priviledges','ni ni-building text-warning','/rolePriviledges',13);
/*!40000 ALTER TABLE `dashboardmenus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mockuserinfo`
--

DROP TABLE IF EXISTS `mockuserinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mockuserinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) DEFAULT NULL,
  `createdOn` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mockuserinfo`
--

LOCK TABLES `mockuserinfo` WRITE;
/*!40000 ALTER TABLE `mockuserinfo` DISABLE KEYS */;
INSERT INTO `mockuserinfo` VALUES (1,'Bob','2019-01-12'),(2,'Bill','2019-02-12'),(3,'Gbenga','2019-03-12'),(4,'Queenth','2019-04-12'),(5,'Loveth','2019-03-12'),(6,'Paul','2019-03-12'),(7,'Emmanuel','2018-12-31'),(8,'Robert','2019-10-13'),(9,'Elizabeth','2019-06-29');
/*!40000 ALTER TABLE `mockuserinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `priviledges`
--

DROP TABLE IF EXISTS `priviledges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `priviledges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priviledge` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `priviledge` (`priviledge`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priviledges`
--

LOCK TABLES `priviledges` WRITE;
/*!40000 ALTER TABLE `priviledges` DISABLE KEYS */;
INSERT INTO `priviledges` VALUES (1,'createAdmin'),(2,'createUser'),(3,'viewCreateUserMenu'),(11,'viewDashboard'),(13,'viewRolePriviledgesMenu'),(4,'viewTablesMenu'),(12,'viewUserProfileMenu');
/*!40000 ALTER TABLE `priviledges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolepriviledges`
--

DROP TABLE IF EXISTS `rolepriviledges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rolepriviledges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleId` int(11) DEFAULT NULL,
  `priviledgeId` int(11) DEFAULT NULL,
  `status` enum('active','disabled') NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roleId` (`roleId`,`priviledgeId`),
  KEY `fk_priviledge` (`priviledgeId`),
  CONSTRAINT `fk_priviledge` FOREIGN KEY (`priviledgeId`) REFERENCES `priviledges` (`id`),
  CONSTRAINT `fk_role` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolepriviledges`
--

LOCK TABLES `rolepriviledges` WRITE;
/*!40000 ALTER TABLE `rolepriviledges` DISABLE KEYS */;
INSERT INTO `rolepriviledges` VALUES (1,1,1,'active','2019-08-28 11:56:44','2019-08-28 11:56:44'),(2,1,2,'active','2019-08-28 11:56:44','2019-08-28 11:56:44'),(3,1,3,'active','2019-08-28 11:56:44','2019-08-28 11:56:44'),(4,1,4,'active','2019-08-28 11:56:44','2019-08-28 11:56:44'),(5,1,11,'active','2019-08-28 11:56:44','2019-08-28 11:56:44'),(6,1,12,'active','2019-08-28 11:56:44','2019-08-28 11:56:44'),(7,1,13,'active','2019-08-28 11:56:44','2019-08-28 11:56:44');
/*!40000 ALTER TABLE `rolepriviledges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roleName` (`roleName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'superAdmin');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `userPass` varchar(255) DEFAULT NULL,
  `userRole` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_userRole` (`userRole`),
  CONSTRAINT `fk_userRole` FOREIGN KEY (`userRole`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Emmanuel Tope-Ojo','etopeojo@gmail.com','Emmanuel@18',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userprofile`
--

DROP TABLE IF EXISTS `userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `userprofile` (
  `userId` int(11) NOT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  UNIQUE KEY `userId` (`userId`),
  CONSTRAINT `fk_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userprofile`
--

LOCK TABLES `userprofile` WRITE;
/*!40000 ALTER TABLE `userprofile` DISABLE KEYS */;
INSERT INTO `userprofile` VALUES (1,'etopeojo','etopeojo@gmail.com','Emmanuel','Tope-Ojo','Lagos, Nigeria','Lagos','Nigeria','','');
/*!40000 ALTER TABLE `userprofile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-04 13:07:23
